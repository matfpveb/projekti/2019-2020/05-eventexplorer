const mongoose = require("mongoose");

const usersSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  name: {
    type: String,
    required: true
  },
  surname: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  username: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  wishList: {
    type: [String],
    default: []
  } 
});

const usersModel = mongoose.model("users", usersSchema);

module.exports = usersModel;
