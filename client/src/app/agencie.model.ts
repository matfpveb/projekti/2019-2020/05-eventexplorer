export interface AgencieModel{
    _id: string;
    name: string;
    contact: string;
    link: string;
}