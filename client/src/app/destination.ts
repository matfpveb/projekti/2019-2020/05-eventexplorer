export interface Destination {
    _id: string;
    agency: string;
    location: string;
    departure: string;
    arrival: string;
    price: number;
    description: string;
    currency: string;
    links: string;
    icons: string;
}
