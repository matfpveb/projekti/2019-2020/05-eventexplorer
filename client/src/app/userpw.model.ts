import { Destination } from './destination';

export interface Userpw {
  name: string;
  surname: string;
  username: string;
  email: string;
  password: string;
  wishList: Destination[];
}
