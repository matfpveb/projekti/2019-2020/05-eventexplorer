import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { AgencieModel } from '../agencie.model';
import { AgenciesService } from '../agencies.service';

@Component({
  selector: 'app-agencies',
  templateUrl: './agencies.component.html',
  styleUrls: ['./agencies.component.css']
})
export class AgenciesComponent implements OnInit, OnDestroy {
  private activeSubscriptions: Subscription[] = [];
  public agencies: Observable<AgencieModel[]>;
  constructor(private agenciesService: AgenciesService) { 
    this.activeSubscriptions.push(this.agenciesService.getAgencies().subscribe((data) =>{
      this.agencies = data;
      console.log(this.agencies);
    }));
  }

  ngOnInit(): void {
  }

  ngOnDestroy(){
    this.activeSubscriptions.map(x => x.unsubscribe());
  }
}
