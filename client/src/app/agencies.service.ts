import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AgenciesService {
  private url = "http://localhost:3000/api/usersRoutes/";
  constructor(private httpClient: HttpClient) { }

  getAgencies(): Observable<any> {
    return this.httpClient.get(this.url + 'agencies');
  }
}
