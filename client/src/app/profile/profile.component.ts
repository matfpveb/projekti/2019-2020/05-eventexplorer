import { Component, OnDestroy, OnInit } from '@angular/core';
import { LoginService } from '../login.service';
import { Destination } from '../destination';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, Subscription } from 'rxjs';
import { Userpw } from '../userpw.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ConfirmedValidator } from '../sign-up/confirmed.validator';
import { ReceiverService } from '../receiver.service';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit, OnDestroy {

  username = '';
  favourites = [];
  public updateProfileHidden: boolean = true;
  private activeSubscriptions: Subscription[] = [];
  public user: Userpw = null;
  form: FormGroup;

  constructor(public loginer: LoginService, private http: HttpClient,
    private formBuilder: FormBuilder, private reciever: ReceiverService) {
      this.form = this.formBuilder.group({
      name: ['', [Validators.pattern('[A-Z][a-z ]+')]],
      surname: ['', [Validators.pattern('[A-Z][a-z ]+')]],
      email: ['', [Validators.email]],
      username: [''],
      password: [''],
      confirm_password: ['']
    }, {
      validator: ConfirmedValidator('password', 'confirm_password')
    });

    this.username = loginer.username;
    this.activeSubscriptions.push(this.http.get('http://localhost:3000/api/usersRoutes/getUser/' + loginer.username).subscribe( (data: Userpw) => {
      if(data){
        this.user = data;
      }
    }));
    console.log('Favourites', this.loginer.favourites);
  }

  get f(){
    return this.form.controls;
  }

  ngOnDestroy(): void {
    this.activeSubscriptions.forEach(x => x.unsubscribe());
  }

  ngOnInit(): void {

  }
  removeDestination(d: Destination){

    let answer = confirm('Are you sure that you want to remove this destination?');
    if(answer) {
      const newLocation = d.location.replace(/ /g, '_');
      const REST_API_SERVER = 'http://localhost:3000/api/usersRoutes/delete/' + this.username + '/' +  d._id + '/';
      this.loginer.favourites = this.loginer.favourites.filter(item => item !== d);

      this.http.delete(REST_API_SERVER).subscribe(
        (val) => {

            console.log('POST call successful value returned in body',
                        val);
        },
        response => {
            console.log('POST call in error', response);


        },
        () => {
            console.log('The POST observable is now completed.');
        });
    }
  }

  submitForm(data) {

    if(!this.form.valid) {
      window.alert("Forma za menjanje informacija nije validna!");
      return;
    }
    let oldUsername = this.user.username;

    const updateProfile = {
      username: data.username === '' ? oldUsername : data.username,
      name: data.name === '' ? this.user.name : data.name,
      surname: data.surname === '' ? this.user.surname : data.surname,
      email: data.email === '' ? this.user.email : data.email,
      password: data.password === '' ? this.user.password : data.password,
    };
    console.log("Update", updateProfile);
    this.activeSubscriptions.push(this.http.put('http://localhost:3000/api/usersRoutes/updateProfile/' + oldUsername, updateProfile).subscribe ( (data:Userpw) => {
      this.user = data;
      window.alert('Uspesno ste azurirali svoj profil!');
    }));
  }

  onUpdateProfile() {
    this.updateProfileHidden = !this.updateProfileHidden;
  }
}
